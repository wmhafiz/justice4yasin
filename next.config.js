/** @type {import('next').NextConfig} */

const STUDIO_REWRITE = {
  source: "/studio/:path*",
  destination:
    process.env.NODE_ENV === "development"
      ? "http://localhost:3333/studio/:path*"
      : "/studio/index.html",
};

const nextConfig = {
  reactStrictMode: true,
  rewrites: () => [STUDIO_REWRITE],
  i18n: {
    locales: ["en", "ms"],
    defaultLocale: "en",
    localeDetection: true,
  },
};

module.exports = nextConfig;
