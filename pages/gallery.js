import { useState } from "react";
import { groq } from "next-sanity";
import Lightbox from "react-awesome-lightbox";
import "react-awesome-lightbox/build/style.css";

import Layout from "../components/layout/layout";
import { urlFor } from "../lib/sanity";
import { getClient } from "../lib/sanity.server";

const query = groq`
{
  "gallery": *[_type == "gallery" && enabled && !(_id in path("drafts.**"))] | order(orderRank) {
    _id,
    title,
    subtitle,
    image,
    fit,
    position
  },
  "siteSettings": *[_type == "siteSettings"][0]{
    ...,
    "tagline1": localTagline1[$lang],
    "tagline2": localTagline2[$lang],
  }
}
`;

export async function getStaticProps({ params, preview = false, locale }) {
  const data = await getClient(preview).fetch(query, { lang: locale });
  if (!data?.gallery) return { notFound: true };

  return {
    props: {
      preview,
      data,
    },
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 10 seconds
    revalidate: 10,
  };
}

export default function Gallery({ data, preview, locale }) {
  const { gallery, siteSettings } = data;
  const [isOpen, setOpen] = useState(false);
  const [imageIndex, setImageIndex] = useState(0);

  const openModal = (index) => {
    setOpen(true);
    setImageIndex(index);
  };

  return (
    <Layout siteSettings={siteSettings} page="Gallery">
      <div className="bg-white px-4 py-6 shadow sm:p-6 sm:rounded-lg mb-4">
        <h1 className="text-lg font-medium mb-6">
          {locale == "en" ? "Gallery" : "Galleri"}
        </h1>
        {isOpen && (
          <Lightbox
            images={gallery.map((file) => {
              let title = file.title;
              if (file.subtitle) {
                title += `: ${file.subtitle}`;
              }
              return {
                url: urlFor(file.image).url(),
                title,
              };
            })}
            startIndex={imageIndex}
            onClose={() => setOpen(false)}
          ></Lightbox>
        )}
        <ul
          role="list"
          className="grid grid-cols-2 gap-x-4 gap-y-8 sm:grid-cols-3 sm:gap-x-6 lg:grid-cols-4 xl:gap-x-8"
        >
          {gallery.map((file, index) => (
            <li key={file._id} className="relative">
              <div className="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                <img
                  src={urlFor(file.image).width(500).height(500).url()}
                  alt=""
                  className={`object-${file.fit} object-${file.position} pointer-events-none group-hover:opacity-75`}
                />
                <button
                  type="button"
                  className="absolute inset-0 focus:outline-none"
                  onClick={() => openModal(index)}
                >
                  <span className="sr-only">View details for {file.title}</span>
                </button>
              </div>
              <p className="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">
                {file.title}
              </p>
              <p className="block text-sm font-medium text-gray-500 pointer-events-none">
                {file.subtitle}
              </p>
            </li>
          ))}
        </ul>
      </div>
    </Layout>
  );
}
