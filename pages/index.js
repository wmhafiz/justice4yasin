import { groq } from "next-sanity";
import Link from "next/link";
import Layout from "../components/layout/layout";
import { urlFor } from "../lib/sanity";
import { getClient } from "../lib/sanity.server";
import Widgets from "../components/widgets/widgets";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";

const query = groq`
{
    "carousel": *[_type == "carousel" && enabled && !(_id in path("drafts.**"))] | order(orderRank) {
        _id,
        title,
        link,
        image
    },
    "featuredPosts": *[_type == "post" && featured && !(_id in path("drafts.**"))] | order(_createdAt desc) [0...2] {
        _id,
        "title": localTitle[$lang],
        "excerpt": localExcerpt[$lang],
        mainImage,
        author->{
          name,
          image
        },
        "url": '/post/' + slug.current
    },
    "newestPosts": *[_type == "post" && !(_id in path("drafts.**"))] | order(_createdAt desc) [0...2] {
        _id,
        "title": localTitle[$lang],
        "excerpt": localExcerpt[$lang],
        body,
        mainImage,
        author->{
          name,
          image
        }, 
        "url": '/post/' + slug.current
    },
    "siteSettings": *[_type == "siteSettings"][0] {
        ...,
        "tagline1": localTagline1[$lang],
        "tagline2": localTagline2[$lang],
    }
}
`;

export async function getStaticProps({ params, preview = false, locale }) {
  const data = await getClient(preview).fetch(query, { lang: locale });

  return {
    props: {
      // Pass down the "preview mode" boolean to the client-side
      preview,
      // Pass down the initial content, and our query
      data,
    },
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 10 seconds
    revalidate: 10,
  };
}

const MyCarousel = ({ carousel }) => {
  return (
    <Carousel
      showArrows={true}
      autoPlay={true}
      infiniteLoop={true}
      //   dynamicHeight={true}
      //   onChange={onChange}
      //   onClickItem={onClickItem}
      //   onClickThumb={onClickThumb}
    >
      {carousel?.map((c) => (
        <div key={c._id} className="relative ">
          <img
            className="overflow-hidden"
            src={urlFor(c.image).url()}
            alt={c.title}
          />
          <p className="absolute overflow-hidden left-0 top-0 h-8 w-full bg-slate-700 text-slate-50 text-lg hover:underline">
            <Link href={c.link.href}>
              <a target="_blank">{c.title}</a>
            </Link>
          </p>
        </div>
      ))}
    </Carousel>
  );
};

export default function AboutUs({ data, preview }) {
  const { carousel, siteSettings, featuredPosts, newestPosts } = data;
  return (
    <Layout siteSettings={siteSettings} page="Home">
      <div className="flex flex-col">
        <div className="shadow-lg rounded-lg mb-4">
          <MyCarousel carousel={carousel} />
        </div>
        <Widgets featuredPosts={featuredPosts} newestPosts={newestPosts} />
      </div>
    </Layout>
  );
}
