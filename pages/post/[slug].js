import React from "react";
import Link from "next/link";
import { groq } from "next-sanity";
import { PortableText } from "@portabletext/react";
import * as dayjs from "dayjs";
import * as relativeTime from "dayjs/plugin/relativeTime";
dayjs.extend(relativeTime);

import Layout from "../../components/layout/layout";
import {
  urlFor,
  usePreviewSubscription,
  filterDataToSingleItem,
  myPortableTextComponents,
} from "../../lib/sanity";
import { getClient } from "../../lib/sanity.server";

const query = groq`
{
    "post": *[_type == "post" && slug.current == $slug] {
      _id,
      _createdAt,
      _updatedAt,
      "title": localTitle[$lang] {
        ...,
        "tagline1": localTagline1[$lang],
        "tagline2": localTagline2[$lang],
      },
      "body": localBody[$lang][]{
        ...,
        markDefs[]{
          ...,
          _type == "internalLink" => {
            "slug": @.reference->slug
          }
        }
      },
      mainImage,
      author->{
        name, 
        role,
        image
     },
      "categories": categories[]->{title},
      "slug": slug.current,
    },
    "siteSettings": *[_type == "siteSettings"][0]
}
`;

const previewQuery = groq`
*[_type == "post" && slug.current == $slug] {
    _id,
    _createdAt,
    _updatedAt,
    "title": localTitle[$lang],
    body[]{
      ...,
      markDefs[]{
        ...,
        _type == "internalLink" => {
          "slug": @.reference->slug
        }
      }
    },,
    mainImage,
    author->{
      name, 
      role,
      image
   },
    "categories": categories[]->{title},
    "slug": slug.current,
}
`;

/**
 * Makes Next.js aware of all the slugs it can expect at this route
 *
 * See how we've mapped over our found slugs to add a `/` character?
 * Idea: Add these in Sanity and enforce them with validation rules :)
 * https://www.simeongriggs.dev/nextjs-sanity-slug-patterns
 */
export async function getStaticPaths({ locales }) {
  const allSlugsQuery = groq`*[_type == "post" && defined(slug.current)][].slug.current`;
  const slugs = await getClient().fetch(allSlugsQuery);
  const paths = slugs
    .map((slug) =>
      locales.map((locale) => ({
        params: { slug },
        locale,
      }))
    )
    .flat();
  return {
    paths,
    fallback: false,
  };
}

/**
 * Fetch the data from Sanity based on the current slug
 *
 * Important: You _could_ query for just one document, like this:
 * *[slug.current == $slug][0]
 * But that won't return a draft document!
 * And you get a better editing experience
 * fetching draft/preview content server-side
 *
 * Also: Ignore the `preview = false` param!
 * It's set by Next.js "Preview Mode"
 * It does not need to be set or changed here
 */
export async function getStaticProps({ params, locale, preview = false }) {
  const queryParams = { slug: params.slug, lang: locale };
  const data = await getClient(preview).fetch(query, queryParams);

  // Escape hatch, if our query failed to return data
  // if (!data) return { notFound: true };

  // Helper function to reduce all returned documents down to just one
  const post = filterDataToSingleItem(data?.post, preview);

  return {
    props: {
      // Pass down the "preview mode" boolean to the client-side
      preview,
      // Pass down the initial content, and our query
      data: {
        post,
        siteSettings: data?.siteSettings,
        query,
        queryParams,
        slug: params.slug,
      },
    },
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 10 seconds
    revalidate: 10,
  };
}

/**
 * The `usePreviewSubscription` takes care of updating
 * the preview content on the client-side
 */
export default function PostDetailPage({ data, preview }) {
  const { data: previewData } = usePreviewSubscription(previewQuery, {
    params: data?.queryParams ?? {},
    // The hook will return this on first render
    // This is why it's important to fetch *draft* content server-side!
    initialData: data?.post,
    // The passed-down preview context determines whether this function does anything
    enabled: preview,
  });

  // Client-side uses the same query, so we may need to filter it down again
  const post = filterDataToSingleItem(previewData, preview);
  const { title, author, _updatedAt, mainImage, body } = post;

  // Notice the optional?.chaining conditionals wrapping every piece of content?
  // This is extremely important as you can't ever rely on a single field
  // of data existing when Editors are creating new documents.
  // It'll be completely blank when they start!
  return (
    <Layout siteSettings={data?.siteSettings} page={title}>
      <div className="relative shadow-lg sm:rounded-lg mb-10">
        <img
          className="w-full max-h-200 object-cover object-center overflow-hidden"
          alt=""
          src={urlFor(mainImage).url()}
        />
        <div className="bg-white px-4 py-6">
          <article aria-labelledby={"post-title-" + title}>
            <div>
              <div className="flex space-x-3">
                <div className="flex-shrink-0">
                  <img
                    className="h-10 w-10 rounded-full"
                    src={urlFor(author.image).url()}
                    alt=""
                  />
                </div>
                <div className="min-w-0 flex-1">
                  <p className="text-sm font-medium text-gray-900">
                    <a href="#" className="hover:underline">
                      {author.name}
                    </a>
                  </p>
                  <p className="text-sm text-gray-500">
                    <a href="#" className="hover:underline">
                      <time dateTime={_updatedAt}>
                        {dayjs(post._createdAt).fromNow()}
                      </time>
                    </a>
                  </p>
                </div>
              </div>
              <h2 className="prose-xl mt-4 font-bold text-gray-900">{title}</h2>
            </div>
            <div className="prose-xl mt-2 text-sm text-gray-700 space-y-4">
              <PortableText
                value={body}
                components={myPortableTextComponents}
              />
            </div>
          </article>
        </div>

        {preview && (
          <div className="fixed bottom-10 right-10">
            <Link href="/api/exit-preview" prefetch={false}>
              <a className="rounded-lg px-4 py-4 text-slate-800 bg-slate-200 hover:bg-slate-400">
                Exit Live Preview
              </a>
            </Link>
          </div>
        )}
      </div>
    </Layout>
  );
}
