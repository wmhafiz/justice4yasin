import React from "react";
import Link from "next/link";
import { groq } from "next-sanity";
import { PortableText } from "@portabletext/react";
import Layout from "../components/layout/layout";
import {
  urlFor,
  usePreviewSubscription,
  filterDataToSingleItem,
  myPortableTextComponents,
} from "../lib/sanity";
import { getClient } from "../lib/sanity.server";
import { useRouter } from "next/router";

const query = groq`
{
  "page": *[_type == "page" && slug.current == $slug] {
    _id,
    "title": localTitle[$lang],
    "subtitle": localSubtitle[$lang],
    "body": localBody[$lang],
    mainImage,
    "slug": slug.current
  },
  "siteSettings": *[_type == "siteSettings"][0] {
    ...,
    "tagline1": localTagline1[$lang],
    "tagline2": localTagline2[$lang],
  },
}
`;

const previewQuery = groq`
*[_type == "page" && slug.current == $slug] {
  _id,
  "title": localTitle[$lang],
  "subtitle": localSubtitle[$lang],
  "body": localBody[$lang],
  mainImage,
  "slug": slug.current
}
`;

/**
 * Makes Next.js aware of all the slugs it can expect at this route
 *
 * See how we've mapped over our found slugs to add a `/` character?
 * Idea: Add these in Sanity and enforce them with validation rules :)
 * https://www.simeongriggs.dev/nextjs-sanity-slug-patterns
 */
export async function getStaticPaths({ locales }) {
  const allSlugsQuery = groq`*[_type == "page" && defined(slug.current)][].slug.current`;
  const slugs = await getClient().fetch(allSlugsQuery);
  const paths = slugs
    .map((slug) =>
      locales.map((locale) => ({
        params: { slug },
        locale,
      }))
    )
    .flat();
  return {
    paths,
    fallback: false,
  };
}

/**
 * Fetch the data from Sanity based on the current slug
 *
 * Important: You _could_ query for just one document, like this:
 * *[slug.current == $slug][0]
 * But that won't return a draft document!
 * And you get a better editing experience
 * fetching draft/preview content server-side
 *
 * Also: Ignore the `preview = false` param!
 * It's set by Next.js "Preview Mode"
 * It does not need to be set or changed here
 */
export async function getStaticProps({ params, preview = false, locale }) {
  const queryParams = { slug: params.slug, lang: locale };
  const data = await getClient(preview).fetch(query, queryParams);

  // Escape hatch, if our query failed to return data
  if (!data) return { notFound: true };

  // Helper function to reduce all returned documents down to just one
  const page = filterDataToSingleItem(data?.page, preview);

  return {
    props: {
      // Pass down the "preview mode" boolean to the client-side
      preview,
      // Pass down the initial content, and our query
      data: {
        page,
        siteSettings: data?.siteSettings,
        query,
        queryParams,
        slug: params.slug,
      },
    },
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 10 seconds
    revalidate: 10,
  };
}

/**
 * The `usePreviewSubscription` takes care of updating
 * the preview content on the client-side
 */
export default function Page({ data, preview }) {
  const router = useRouter();
  const { locale } = router;
  const { data: previewData } = usePreviewSubscription(previewQuery, {
    params: data?.queryParams ?? {},
    // The hook will return this on first render
    // This is why it's important to fetch *draft* content server-side!
    initialData: data?.page,
    // The passed-down preview context determines whether this function does anything
    enabled: preview,
  });

  // Client-side uses the same query, so we may need to filter it down again
  const page = filterDataToSingleItem(previewData, preview);
  const { title, subtitle, mainImage, body } = page;

  // Notice the optional?.chaining conditionals wrapping every piece of content?
  // This is extremely important as you can't ever rely on a single field
  // of data existing when Editors are creating new documents.
  // It'll be completely blank when they start!
  return (
    <Layout siteSettings={data?.siteSettings} page={page.title}>
      <div className="relative shadow-lg sm:rounded-lg mb-10">
        <img
          className="w-full max-h-200 object-cover object-center overflow-hidden"
          alt=""
          src={urlFor(mainImage).url()}
        />
        <div className="bg-white px-4 py-6">
          <div className="text-lg max-w-prose mx-auto">
            <h1>
              <span className="block text-base text-center text-rose-600 font-semibold tracking-wide uppercase">
                {title}
              </span>
              <span className="mt-2 block text-3xl text-center leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl">
                {subtitle}
              </span>
            </h1>
            <div className="mt-8 text-xl text-gray-500 leading-8">
              <article className="prose-xl">
                <PortableText
                  value={body}
                  components={myPortableTextComponents}
                />
              </article>
            </div>
          </div>
        </div>

        {preview && (
          <div className="fixed bottom-10 right-10">
            <Link href="/api/exit-preview" prefetch={false}>
              <a className="rounded-lg px-4 py-4 text-slate-800 bg-slate-200 hover:bg-slate-400">
                Exit Live Preview
              </a>
            </Link>
          </div>
        )}
      </div>
    </Layout>
  );
}
