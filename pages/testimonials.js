import { groq } from "next-sanity";
import { getClient } from "../lib/sanity.server";
import Layout from "../components/layout/layout";
import Testimonial from "../components/testimonial";

const query = groq`
{
  "testimonials": *[_type == "testimonial" && !(_id in path("drafts.**"))] {
    _id,
    title,
    body,
    name,
    image,
    url
  },
  "siteSettings": *[_type == "siteSettings"][0] {
    ...,
    "tagline1": localTagline1[$lang],
    "tagline2": localTagline2[$lang],
  }
}
`;

const previewQuery = groq`
*[_type == "testimonial" && !(_id in path("drafts.**"))] {
  _id,
  title,
  body,
  name,
  image,
  url
}
`;

export async function getStaticProps({ params, preview = false, locale }) {
  const data = await getClient(preview).fetch(query, { lang: locale });
  if (!data?.testimonials) return { notFound: true };
  return {
    props: {
      preview,
      data,
    },
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 10 seconds
    revalidate: 10,
  };
}

export default function TestimonialPage({ data, preview }) {
  const { testimonials, siteSettings } = data;

  return (
    <Layout siteSettings={siteSettings} page="Testimonials">
      <div className="mt-4">
        <h1 className="sr-only">Public Figures Testimonials</h1>
        <ul role="list" className="space-y-4">
          {testimonials &&
            testimonials.map((t) => <Testimonial key={t._id} {...t} />)}
        </ul>
      </div>
    </Layout>
  );
}
