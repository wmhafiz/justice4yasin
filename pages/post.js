import { PortableText } from "@portabletext/react";
import Link from "next/link";
import { groq } from "next-sanity";
import Layout from "../components/layout/layout";
import { urlFor, myPortableTextComponents } from "../lib/sanity";
import { getClient } from "../lib/sanity.server";
import * as dayjs from "dayjs";
import * as relativeTime from "dayjs/plugin/relativeTime";
import { useRouter } from "next/router";
dayjs.extend(relativeTime);

const query = groq`
{
    "posts": *[_type == "post" && !(_id in path("drafts.**"))] | order(_createdAt desc) {
      _id,
      _createdAt,
      _updatedAt,
      "title": localTitle[$lang],
      "excerpt": localExcerpt[$lang],
      "body": localBody[$lang],
      body,
      mainImage,
      author->{
        name, 
        role,
        image
     },
      "categories": categories[]->{title},
      "slug": slug.current,
    },
    "siteSettings": *[_type == "siteSettings"][0] {
      ...,
      "tagline1": localTagline1[$lang],
      "tagline2": localTagline2[$lang],
    }
}
`;

export async function getStaticProps({ params, preview = false, locale }) {
  const data = await getClient(preview).fetch(query, { lang: locale });

  return {
    props: {
      // Pass down the "preview mode" boolean to the client-side
      preview,
      // Pass down the initial content, and our query
      data: {
        posts: data?.posts,
        siteSettings: data?.siteSettings,
      },
    },
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 10 seconds
    revalidate: 10,
  };
}

export default function Posts({ data, preview }) {
  const { locale } = useRouter();
  const { posts, siteSettings } = data;

  return (
    <Layout siteSettings={siteSettings} page="Posts">
      <div className="relative pt-16 pb-20 px-4 sm:px-6 lg:pt-24 lg:pb-28 lg:px-8">
        <div className="absolute inset-0">
          <div className="bg-white h-1/3 sm:h-2/3" />
        </div>
        <div className="relative max-w-7xl mx-auto">
          <div className="text-center">
            <h2 className="text-3xl tracking-tight font-extrabold text-gray-900 sm:text-4xl">
              {locale == "en" ? "From the blog" : "Artikel"}
            </h2>
            <p className="mt-3 max-w-2xl mx-auto text-xl text-gray-500 sm:mt-4">
              Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ipsa
              libero labore natus atque, ducimus sed.
            </p>
          </div>
          <div className="mt-12 max-w-lg mx-auto grid gap-5 lg:grid-cols-3 lg:max-w-none">
            {posts.map((post) => (
              <Link key={post._id} href={`/post/${post.slug}`} locale={locale}>
                <a>
                  <div
                    key={post.title}
                    className="flex flex-col rounded-lg shadow-lg overflow-hidden"
                  >
                    <div className="flex-shrink-0">
                      <img
                        className="h-48 w-full object-cover"
                        src={urlFor(post.mainImage).url()}
                        alt=""
                      />
                    </div>
                    <div className="flex-1 bg-white p-6 flex flex-col justify-between">
                      <div className="flex-1">
                        <p className="text-sm font-medium text-indigo-600">
                          <a href="#" className="hover:underline">
                            {post.categories[0].title}
                          </a>
                        </p>
                        <a href={post.href} className="block mt-2">
                          <p className="text-xl font-semibold text-gray-900">
                            {post.title}
                          </p>
                          <div className="mt-2 text-sm text-gray-700 space-y-4">
                            <PortableText
                              value={post.excerpt}
                              components={myPortableTextComponents}
                            />
                          </div>
                        </a>
                      </div>
                      <div className="mt-6 flex items-center">
                        <div className="flex-shrink-0">
                          <a href="#">
                            <span className="sr-only">{post.author.name}</span>
                            <img
                              className="h-10 w-10 rounded-full"
                              src={urlFor(post.author.image).maxWidth(50).url()}
                              alt=""
                            />
                          </a>
                        </div>
                        <div className="ml-3">
                          <p className="text-sm font-medium text-gray-900">
                            <a href="#" className="hover:underline">
                              {post.author.name}
                            </a>
                          </p>
                          <div className="flex space-x-1 text-sm text-gray-500">
                            <time dateTime={post._updatedAt}>
                              {dayjs(post._createdAt).fromNow()}
                            </time>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </a>
              </Link>
            ))}
          </div>
        </div>
      </div>
    </Layout>
  );
}
