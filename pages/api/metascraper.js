import Cors from "cors";
import got from "got";
import initMiddleware from "../../lib/init-Middleware";

const metascraper = require("metascraper")([
  require("metascraper-instagram")(),
  // require("metascraper-media-provider")(),
  require("metascraper-author")(),
  require("metascraper-date")(),
  require("metascraper-description")(),
  require("metascraper-image")(),
  require("metascraper-logo")(),
  require("metascraper-clearbit")(),
  require("metascraper-publisher")(),
  require("metascraper-title")(),
  require("metascraper-url")(),
  require("metascraper-iframe")(),
  require("metascraper-manifest")(),
]);

// Initialize the cors middleware
const cors = initMiddleware(
  // You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
  Cors({
    // Only allow requests with GET, POST and OPTIONS
    methods: ["GET", "POST", "OPTIONS"],
  })
);

export default async function scrape(req, res) {
  // Run cors
  await cors(req, res);

  if (!req?.query?.secret) {
    return res.status(401).json({ message: "No secret token" });
  }

  if (req.query.secret !== process.env.SANITY_PREVIEW_SECRET) {
    return res.status(401).json({ message: "Invalid secret token" });
  }

  const targetUrl = req.query?.url;
  //   const targetUrl = 'https://m.facebook.com/story.php?story_fbid=104927605530819&id=101478505875729'

  const { body: html, url } = await got(targetUrl);
  const metadata = await metascraper({ html, url });
  console.log(metadata);

  res.status(200).json(metadata);
}
