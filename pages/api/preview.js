export default function preview(req, res) {
  if (!req?.query?.secret) {
    return res.status(401).json({ message: "No secret token" });
  }

  // Check the secret and next parameters
  // This secret should only be known to this API route and the CMS
  if (req.query.secret !== process.env.SANITY_PREVIEW_SECRET) {
    return res.status(401).json({ message: "Invalid secret token" });
  }

  if (!req.query.slug) {
    return res.status(401).json({ message: "No slug" });
  }

  if (!req.query.type) {
    return res.status(401).json({ message: "No doc type" });
  }

  // Enable Preview Mode by setting the cookies
  res.setPreviewData({});

  // Redirect to the path from the fetched post
  // We don't redirect to req.query.slug as that might lead to open redirect vulnerabilities
  if (req?.query?.type == "page") {
    res.writeHead(307, { Location: `/${req?.query?.slug}` ?? `/` });
  } else if (req.query.type == "post") {
    res.writeHead(307, { Location: `/post/${req?.query?.slug}` ?? `/post` });
  }

  return res.end();
}
