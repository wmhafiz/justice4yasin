import { Client } from "peekalink";

const client = new Client({ apiKey: "232db9d2-9790-4885-9af1-f15e4d631139" });

async function isAvailable(req, res) {
  if (!req?.query?.secret) {
    return res.status(401).json({ message: "No secret token" });
  }

  if (req.query.secret !== process.env.SANITY_PREVIEW_SECRET) {
    return res.status(401).json({ message: "Invalid secret token" });
  }

  const availability = await client.availability(req.query?.url);
  res.status(200).json(availability);
}

export default async function preview(req, res) {
  if (!req?.query?.secret) {
    return res.status(401).json({ message: "No secret token" });
  }

  if (req.query.secret !== process.env.SANITY_PREVIEW_SECRET) {
    return res.status(401).json({ message: "Invalid secret token" });
  }

  const data = await client.preview(req.query?.url);
  res.status(200).json(data);
}
