export default function exit(req, res) {
  console.log("exiting preview mode..");
  res.clearPreviewData();
  console.log("query", JSON.stringify(req.query, null, 2));
  res.writeHead(307, { Location: req?.query?.slug ?? `/` });
}
