import { useState } from "react";
import { groq } from "next-sanity";
import Lightbox from "react-awesome-lightbox";
import "react-awesome-lightbox/build/style.css";
import { urlFor } from "../lib/sanity";
import { getClient } from "../lib/sanity.server";
import Layout from "../components/layout/layout";

const query = groq`
{
  "people": *[_type == "person" && !(_id in path("drafts.**"))] | order(orderRank) {
    name,
    "role": localRole[$lang],
    "bio": localBio[$lang],
    image
  },
  "siteSettings": *[_type == "siteSettings"][0] {
    ...,
    "tagline1": localTagline1[$lang],
    "tagline2": localTagline2[$lang],
  }
}
`;

export async function getStaticProps({ params, preview = false, locale }) {
  const data = await getClient(preview).fetch(query, { lang: locale });

  // Escape hatch, if our query failed to return data
  if (!data?.people) return { notFound: true };

  return {
    props: {
      // Pass down the "preview mode" boolean to the client-side
      preview,
      // Pass down the initial content, and our query
      data,
      locale,
    },
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 10 seconds
    revalidate: 10,
  };
}

export default function AboutUs({ data, preview, locale }) {
  const { people, siteSettings } = data;
  const [isOpen, setOpen] = useState(false);
  const [imageIndex, setImageIndex] = useState(0);
  console.log({ locale });
  const openModal = (index) => {
    setOpen(true);
    setImageIndex(index);
  };

  return (
    <Layout siteSettings={siteSettings} page="About Us">
      {isOpen && (
        <Lightbox
          images={people.map((person) => {
            return {
              url: urlFor(person?.image).url(),
              title: `${person?.name} - ${person?.role}`,
            };
          })}
          startIndex={imageIndex}
          onClose={() => setOpen(false)}
        ></Lightbox>
      )}
      <div className="bg-white px-4 py-6 shadow sm:p-6 sm:rounded-lg mb-4">
        <div className="bg-white">
          <div className="max-w-7xl mx-auto py-12 px-4 text-center sm:px-6 lg:px-8">
            <div className="space-y-8 sm:space-y-12">
              <div className="space-y-5 sm:mx-auto sm:max-w-xl sm:space-y-4 lg:max-w-5xl">
                <h2 className="text-3xl font-bold tracking-tight sm:text-4xl">
                  {locale === "en" ? "About Us" : "Tentang Kami"}
                </h2>
                <p className="text-xl text-gray-500">
                  {locale === "en"
                    ? "Yasin Sulaiman's Support System"
                    : "Sistem Sokongan Yasin Sulaiman"}
                </p>
              </div>
              <ul
                role="list"
                className="mx-auto grid grid-cols-2 gap-x-4 gap-y-8 sm:grid-cols-4 md:gap-x-6 lg:max-w-5xl lg:gap-x-8 lg:gap-y-12 xl:grid-cols-6"
              >
                {people.map(
                  (person, index) =>
                    person?.image && (
                      <li key={person.name}>
                        <div
                          className="space-y-4 cursor-pointer"
                          onClick={() => openModal(index)}
                        >
                          <img
                            className="mx-auto h-20 w-20 rounded-full lg:w-24 lg:h-24 hover:shadow-lg ring-amber-400 ring-offset-4 hover:ring-4"
                            src={urlFor(person.image).url()}
                            alt={person.name}
                          />
                          <div className="space-y-2">
                            <div className="text-xs font-medium lg:text-sm">
                              <h3>{person.name}</h3>
                              <p className="text-rose-600">{person.role}</p>
                            </div>
                          </div>
                        </div>
                      </li>
                    )
                )}
              </ul>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}
