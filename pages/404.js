import Head from "next/head";
import Link from "next/link";
import Layout from "../components/layout/layout";
import { groq } from "next-sanity";
import { getClient } from "../lib/sanity.server";

const query = groq`
{
  "siteSettings": *[_type == "siteSettings"][0] {
    ...,
    "tagline1": localTagline1[$lang],
    "tagline2": localTagline2[$lang],
  }
}
`;

export async function getStaticProps({ params, preview = false, locale }) {
  const data = await getClient(preview).fetch(query, { lang: locale });
  return {
    props: {
      preview,
      data,
      locale,
    },
  };
}

export default function Custom404({ data, locale }) {
  const { siteSettings } = data;

  return (
    <Layout siteSettings={siteSettings} page="404 Not Found">
      <Head>
        <title>YasinSulaiman.com: Not Found</title>
        <meta name="description" content="YasinSulaiman.com" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className="bg-white px-6 py-6 shadow-lg rounded-lg">
        <main className="sm:flex mx-10 my-48">
          <p className="text-4xl font-extrabold text-rose-600 sm:text-5xl">
            404
          </p>
          <div className="sm:ml-6">
            <div className="sm:border-l sm:border-gray-200 sm:pl-6">
              <h1 className="text-4xl font-extrabold text-gray-900 tracking-tight sm:text-5xl">
                Page not found
              </h1>
              <p className="mt-1 text-base text-gray-500">
                Please check the URL in the address bar and try again.
              </p>
            </div>
            <div className="mt-10 flex space-x-3 sm:border-l sm:border-transparent sm:pl-6">
              <Link href="/" locale={locale}>
                <a className="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-rose-600 hover:bg-rose-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                  Go back home
                </a>
              </Link>
              <a
                href="#"
                className="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md text-rose-700 bg-rose-100 hover:bg-rose-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                Contact support
              </a>
            </div>
          </div>
        </main>
      </div>
    </Layout>
  );
}
