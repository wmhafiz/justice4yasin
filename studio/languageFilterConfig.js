export const supportedLanguages = [
  { id: "en", title: "English", isDefault: true },
  { id: "ms", title: "Malay" },
];

export const baseLanguage = supportedLanguages.find((l) => l.isDefault);

export default {
  supportedLanguages: [
    { id: "en", title: "English" },
    { id: "ms", title: "Malay" },
    //...
  ],
  // Select Norwegian (Bokmål) by default
  defaultLanguages: ["en"],
  // Only show language filter for document type `page` (schemaType.name)
  documentTypes: ["donation", "post", "person", "page", "siteSettings"],
  filterField: (enclosingType, field, selectedLanguageIds) =>
    !enclosingType.name.startsWith("locale") ||
    selectedLanguageIds.includes(field.name),
};
