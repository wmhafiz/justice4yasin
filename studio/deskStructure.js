import React from "react";
import ReactJson from "react-json-view";
import { HiOutlineUserGroup } from "react-icons/hi";
import { AiOutlineSetting } from "react-icons/ai";
import { MdTitle, MdOutlinePhoto } from "react-icons/md";
import { GrGallery } from "react-icons/gr";
import S from "@sanity/desk-tool/structure-builder";
import Iframe from "sanity-plugin-iframe-pane";
import { orderableDocumentListDeskItem } from "@sanity/orderable-document-list";
import resolveProductionUrl from "./resolveProductionUrl";

const JsonPreview = ({ document }) => (
  <>
    <h1>JSON Data for &quot;{document.displayed.title}&quot;</h1>
    {/* <pre>{JSON.stringify(document.displayed, null, 2)}</pre> */}
    <ReactJson src={document.displayed} />
  </>
);

export const getDefaultDocumentNode = ({ documentId, schemaType }) => {
  const JsonPreviewView = S.view.component(JsonPreview).title("JSON");

  if (schemaType === "page") {
    return S.document().views([
      S.view.form(),
      JsonPreviewView,
      S.view
        .component(Iframe)
        .options({
          // Required: Accepts an async function
          url: (doc) => resolveProductionUrl(doc),
          // Optional: Set the default size
          // defaultSize: `mobile`, // default `desktop`
          // Optional: Add a reload button, or reload on new document revisions
          reload: {
            button: true, // default `undefined`
            revision: true, // default `undefined`
          },
        })
        .title("Preview"),
    ]);
  } else if (schemaType === "post") {
    return S.document().views([
      S.view.form(),
      JsonPreviewView,
      S.view
        .component(Iframe)
        .options({
          // Required: Accepts an async function
          url: (doc) => resolveProductionUrl(doc),
          // Optional: Set the default size
          // defaultSize: `mobile`, // default `desktop`
          // Optional: Add a reload button, or reload on new document revisions
          reload: {
            button: true, // default `undefined`
            revision: true, // default `undefined`
          },
        })
        .title("Preview"),
    ]);
  } else if (schemaType === "siteSettings") {
    return S.document().views([S.view.form(), JsonPreviewView]);
  }
};

export default () =>
  S.list()
    .title("Content")
    .items([
      ...S.documentTypeListItems().filter(
        (item) =>
          ![
            "Site Settings",
            "Media Tag",
            "Person",
            "Gallery",
            "Carousel",
          ].includes(item.getTitle())
      ),
      orderableDocumentListDeskItem({
        type: "gallery",
        title: "Gallery",
        icon: GrGallery,
      }),
      orderableDocumentListDeskItem({
        type: "carousel",
        title: "Carousel",
        icon: MdOutlinePhoto,
      }),
      orderableDocumentListDeskItem({
        type: "person",
        title: "People",
        icon: HiOutlineUserGroup,
      }),
      S.divider(),
      S.listItem()
        .title("Settings")
        .icon(AiOutlineSetting)
        .child(
          S.list()
            .title("Settings")
            .items([
              S.listItem()
                .title("Metadata")
                .icon(MdTitle)
                .child(
                  S.document()
                    .schemaType("siteSettings")
                    .documentId("siteSetting")
                ),
              // S.documentTypeListItem("page"),
              // S.listItem()
              //   .title('Main Navigation')
              //   .child(
              //     S.document()
              //       .schemaType('navigation')
              //       .documentId('navigation')
              //   )
            ])
        ),
    ]);
