const previewSecret = "JUSTICE4YASIN"; // Copy the string you used for SANITY_PREVIEW_SECRET

// Replace `remoteUrl` with your deployed Next.js site
const remoteUrl = `https://yasinsulaiman.com`;
const localUrl = `http://localhost:3000`;

export default function resolveProductionUrl(doc) {
  const baseUrl =
    window.location.hostname === "localhost" ? localUrl : remoteUrl;

  const previewUrl = new URL(baseUrl);
  previewUrl.pathname = `/api/preview`;
  previewUrl.searchParams.append(`secret`, previewSecret);
  previewUrl.searchParams.append(`slug`, doc?.slug?.current ?? `/`);
  previewUrl.searchParams.append(`type`, doc?._type ?? "");
  return previewUrl.toString();

  // if (doc._type == "page") {
  //   return `${baseUrl}/${doc?.slug?.current}`;
  // } else {
  //   return `${baseUrl}/post/${doc?.slug?.current}`;
  // }
}
