import React from "react";

export default function Card({ value }) {
  return (
    <div class="max-w-sm rounded overflow-hidden shadow-lg">
      <img class="w-full" src={value.image.url} alt={value.title} />
      <div class="px-6 py-4">
        <div class="font-bold text-xl mb-2">{value.title}</div>
        <p class="text-gray-700 text-base">{value.subtitle}</p>
        <a href={value.url}>Read article..</a>
      </div>
    </div>
  );
}
