import React from "react";
import PropTypes from "prop-types";
import getVideoId from "get-video-id";

export default function YoutubePreview({ value }) {
  if (!value?.url) {
    return null;
  }
  const { id } = getVideoId(value.url);
  const url = `https://www.youtube.com/embed/${id}`;
  return (
    <iframe
      width="400"
      height="250"
      src={url}
      title="Youtube Preview"
      frameBorder={0}
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      allowFullScreen
    ></iframe>
  );
};

YoutubePreview.propTypes = {
  value: PropTypes.object,
};