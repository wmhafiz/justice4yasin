import T from "@sanity/base/initial-value-template-builder";

export default [
  ...T.defaults(),
  T.template({
    id: "authorWithRole",
    title: "Author with title",
    schemaType: "article",
    value: (parameters) => ({
      description: JSON.stringify(parameters, null, 2),
    }),
  }),
];
