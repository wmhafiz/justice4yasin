import React from "react";
import { FaFacebook } from "react-icons/fa";

const FBPagePreview = ({ value }) => {
  if (!value?.url) {
    return null;
  }
  // return <pre>{JSON.stringify(value, null, 2)}</pre>;
  const { url, width, height } = value;
  const encodedUrl = encodeURIComponent(url);
  return (
    <iframe
      className="rounded-lg"
      src={`https://www.facebook.com/plugins/page.php?href=${encodedUrl}&tabs=timeline&width=${width}&height=${height}&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=650085618343609`}
      width={width}
      height={height}
      style={{ border: "none", overflow: "hidden" }}
      scrolling="no"
      frameBorder="0"
      allowFullScreen={true}
      allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"
    ></iframe>
  );
};

export default {
  name: "facebook-page",
  type: "object",
  title: "FB Page",
  icon: FaFacebook,
  fields: [
    {
      name: "url",
      type: "url",
      title: "FB Page URL",
      initialValue: "https://www.facebook.com/yasinsulaimanofficial",
    },
    {
      name: "width",
      type: "number",
      initialValue: 500,
    },
    {
      name: "height",
      type: "number",
      initialValue: 800,
    },
  ],
  preview: {
    select: {
      url: "url",
      width: "width",
      height: "height",
    },
    component: FBPagePreview,
  },
};
