import React, { useState, useEffect } from "react";
import { BsNewspaper } from "react-icons/bs";
// import ArticleCustomInput from "../components/ArticleCustomInput";

// const apiBaseURL =
//   process.env.NODE_ENV == "development"
//     ? "http://localhost:3000"
//     : "https://yasinsulaiman.com";

// const UrlPreview = ({ value }) => {
//   const [data, setData] = useState({});
//   const url = `${apiBaseURL}/api/metascraper/?secret=JUSTICE4YASIN&url=${encodeURIComponent(
//     value?.url
//   )}`;
//   console.log({ url });

//   useEffect(() => {
//     const getData = async () => {
//       const res = await fetch(url, { mode: "no-cors" }).then((res) =>
//         res.json()
//       );
//       setData(res);
//     };
//     getData();
//   }, [value]);

//   return <pre>{JSON.stringify(value, null, 2)}</pre>;
// };

export default {
  name: "article",
  type: "object",
  title: "Article",
  icon: BsNewspaper,
  // initialValue: () => ({
  //   publishedAt: new Date().toISOString(),
  // }),
  fields: [
    {
      name: "url",
      title: "URL",
      type: "urlWithMetadata",
      options: {
        collapsed: true,
      },
      // inputComponent: ArticleCustomInput,
    },
  ],

  preview: {
    select: {
      title: "url.meta.title",
      subtitle: "url.openGraph.siteName",
      imageUrl: "url.openGraph.image",
    },
    prepare({ title, subtitle, imageUrl }) {
      return {
        title: title,
        subtitle: subtitle,
        media: <img alt={title} src={imageUrl} />,
      };
    },
    // component: ArticlePreview,
  },
};
