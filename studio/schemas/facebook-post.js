import React from "react";
import { FaFacebookSquare } from "react-icons/fa";

const FBPostPreview = ({ value }) => {
  if (!value?.url) {
    return null;
  }
  // return <pre>{JSON.stringify(value, null, 2)}</pre>;
  const { url, width, height } = value;
  const encodedUrl = encodeURIComponent(url);
  return (
    <iframe
      className="rounded-lg"
      src={`https://www.facebook.com/plugins/post.php?href=${encodedUrl}&width=${width}&show_text=true&appId=650085618343609&height=${height}`}
      width={width}
      height={height}
      style={{ border: "none", overflow: "hidden" }}
      scrolling="no"
      frameBorder="0"
      allowFullScreen={true}
      allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"
    ></iframe>
  );
};

export default {
  name: "facebook-post",
  type: "object",
  title: "FB Post",
  icon: FaFacebookSquare,
  fields: [
    {
      name: "url",
      type: "url",
      title: "FB Post URL",
      initialValue:
        "https://www.facebook.com/story.php?story_fbid=104936102196636&id=101478505875729",
    },
    {
      name: "width",
      type: "number",
      initialValue: 500,
    },
    {
      name: "height",
      type: "number",
      initialValue: 513,
    },
  ],
  preview: {
    select: {
      url: "url",
      width: "width",
      height: "height",
    },
    component: FBPostPreview,
  },
};
