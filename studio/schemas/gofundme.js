import React from "react";
import { SiGofundme } from "react-icons/si";

const GoFundMePreview = ({ value }) => {
  if (!value?.url) {
    return null;
  }
  // return <pre>{JSON.stringify(value, null, 2)}</pre>;
  const { url, size } = value;
  return (
    <iframe
      className="gfm-embed h-64 mt-4"
      src={`${url}/widget/${size}/`}
    ></iframe>
  );
};

export default {
  name: "gofundme",
  type: "object",
  title: "GoFundMe",
  icon: SiGofundme,
  fields: [
    {
      name: "url",
      type: "url",
      title: "GoFundMe URL",
      initialValue:
        "https://www.gofundme.com/f/by7hs-freedom-for-yasin-sulaiman",
    },
    {
      name: "size",
      type: "string",
      options: {
        list: ["small", "medium", "large"],
        layout: "radio",
      },
      initialValue: "large",
    },
  ],
  preview: {
    select: {
      url: "url",
      size: "size",
    },
    component: GoFundMePreview,
  },
};
