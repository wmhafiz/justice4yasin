import { HiOutlinePencil } from "react-icons/hi";
import { baseLanguage } from "../languageFilterConfig";

export default {
  name: "post",
  title: "Post",
  type: "document",
  icon: HiOutlinePencil,
  initialValue: () => ({
    featured: false,
    publishedAt: new Date().toISOString(),
  }),
  fields: [
    {
      name: "localTitle",
      title: "Title",
      type: "localeString",
    },
    {
      name: "slug",
      title: "Slug",
      type: "slug",
      options: {
        source: `localTitle.${baseLanguage.id}`,
        maxLength: 96,
      },
    },
    {
      name: "author",
      title: "Author",
      type: "reference",
      to: { type: "person" },
    },
    {
      name: "mainImage",
      title: "Main image",
      type: "image",
      options: {
        hotspot: true,
      },
    },
    {
      name: "categories",
      title: "Categories",
      type: "array",
      of: [{ type: "reference", to: { type: "category" } }],
    },
    {
      name: "publishedAt",
      title: "Published at",
      type: "datetime",
    },
    {
      name: "localExcerpt",
      title: "Excerpt",
      type: "localeRichText",
    },
    {
      name: "localBody",
      title: "Body",
      type: "localeRichText",
    },
    {
      name: "featured",
      title: "Featured",
      type: "boolean",
    },
  ],

  preview: {
    select: {
      title: `localTitle.${baseLanguage.id}`,
      author: "author.name",
      media: "mainImage",
    },
    prepare(selection) {
      const { author } = selection;
      return Object.assign({}, selection, {
        subtitle: author && `by ${author}`,
      });
    },
  },
};
