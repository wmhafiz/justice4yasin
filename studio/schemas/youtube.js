import { FaYoutube } from "react-icons/fa";
import YoutubePreview from "../components/youtube-preview";

export default {
  name: "youtube",
  type: "object",
  title: "Youtube",
  icon: FaYoutube,
  fields: [
    {
      name: "url",
      type: "url",
      title: "Youtube URL",
    },
  ],
  preview: {
    select: {
      url: "url",
    },
    component: YoutubePreview,
  },
};
