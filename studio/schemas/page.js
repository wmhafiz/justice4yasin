import { MdOutlineArticle } from "react-icons/md";
import { baseLanguage } from "../languageFilterConfig";

export default {
  name: "page",
  title: "Page",
  type: "document",
  icon: MdOutlineArticle,
  fields: [
    {
      name: "localTitle",
      title: "Title",
      type: "localeString",
    },
    {
      name: "localSubtitle",
      title: "Subtitle",
      type: "localeString",
    },
    {
      name: "slug",
      title: "Slug",
      type: "slug",
      options: {
        source: "title",
        maxLength: 96,
      },
    },
    {
      name: "mainImage",
      title: "Main image",
      type: "image",
      options: {
        hotspot: true,
      },
    },
    {
      name: "updatedAt",
      title: "Updated at",
      type: "datetime",
    },
    {
      name: "localBody",
      title: "Body",
      type: "localeRichText",
    },
  ],

  preview: {
    select: {
      title: `localTitle.${baseLanguage.id}`,
      slug: "slug.current",
      media: "mainImage",
    },
    prepare(selection) {
      const { slug } = selection;
      return Object.assign({}, selection, {
        subtitle: slug && `/${slug}`,
      });
    },
  },
};
