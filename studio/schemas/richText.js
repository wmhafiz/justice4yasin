import { BiLinkExternal, BiLink } from "react-icons/bi";
import { MdAddPhotoAlternate } from "react-icons/md";

export default {
  name: "richText",
  type: "array",
  of: [
    {
      type: "block",
      marks: {
        annotations: [
          {
            name: "link",
            type: "object",
            title: "External link",
            fields: [
              {
                name: "href",
                type: "url",
                title: "URL",
              },
              {
                title: "Open in new tab",
                name: "blank",
                type: "boolean",
                default: true,
              },
            ],
            blockEditor: {
              icon: BiLinkExternal,
            },
          },
          {
            name: "internalLink",
            type: "object",
            title: "Internal link",
            fields: [
              {
                name: "reference",
                type: "reference",
                title: "Reference",
                to: [
                  { type: "post" },
                  // { type: "page" }
                ],
              },
            ],
            blockEditor: {
              icon: BiLink,
            },
          },
        ],
      },
    },
    {
      type: "youtube",
    },
    {
      type: "article",
    },
    {
      type: "facebook-page",
    },
    {
      type: "facebook-post",
    },
    {
      type: "gofundme",
    },
    {
      type: "image",
      icon: MdAddPhotoAlternate,
    },
  ],
};
