import { supportedLanguages } from "../languageFilterConfig";
import StringWithLimits from "../components/StringWithLimitsInput";

const localeString = {
  title: "Localized string",
  name: "localeString",
  type: "object",
  // Fieldsets can be used to group object fields.
  // Here we omit a fieldset for the "default language",
  // making it stand out as the main field.
  fieldsets: [
    {
      title: "Translations",
      name: "translations",
      options: { collapsible: true },
    },
  ],
  // Dynamically define one field per language
  fields: supportedLanguages.map((lang) => ({
    title: lang.title,
    name: lang.id,
    type: "string",
    inputComponent: StringWithLimits,
    validation: (Rule) => Rule.max(60),
    fieldset: lang.isDefault ? null : "translations",
  })),
};

export default localeString;
