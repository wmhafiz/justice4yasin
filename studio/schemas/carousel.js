import {
  orderRankField,
  orderRankOrdering,
} from "@sanity/orderable-document-list";

export default {
  name: "carousel",
  title: "Carousel",
  type: "document",
  orderings: [orderRankOrdering],
  fields: [
    orderRankField({ type: "category" }),
    {
      name: "title",
      title: "Title",
      type: "string",
    },
    {
      name: "image",
      title: "Image",
      type: "image",
      options: {
        hotspot: true,
      },
    },
    {
      name: "enabled",
      type: "boolean",
      title: "Enabled",
    },
    {
      name: "link",
      type: "object",
      title: "External link",
      fields: [
        {
          name: "href",
          type: "url",
          title: "URL",
        },
        {
          title: "Open in new tab",
          name: "blank",
          type: "boolean",
          default: true,
        },
      ],
    },
  ],
  preview: {
    select: {
      title: "title",
      subtitle: "link.href",
      media: "image",
    },
  },
};
