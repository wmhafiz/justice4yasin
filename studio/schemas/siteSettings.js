import { AiOutlineSetting } from "react-icons/ai";

export default {
  name: "siteSettings",
  title: "Site Settings",
  type: "document",
  icon: AiOutlineSetting,
  __experimental_actions: [/*'create',*/ "update", /*'delete',*/ "publish"],
  fields: [
    {
      name: "title",
      title: "Site Title",
      type: "string",
    },
    {
      name: "description",
      title: "Meta Description",
      type: "text",
    },
    {
      name: "tagline1",
      title: "Tagline 1",
      type: "string",
    },
    {
      name: "localTagline1",
      title: "Tagline 1",
      type: "localeString",
    },
    {
      name: "tagline2",
      title: "Tagline 2",
      type: "string",
    },
    {
      name: "localTagline2",
      title: "Tagline 2",
      type: "localeString",
    },
    {
      name: "facebook",
      title: "Facebook Link",
      type: "url",
    },
    {
      name: "instagram",
      title: "Instagram Link",
      type: "url",
    },
    {
      name: "gofundme",
      title: "Gofundme Link",
      type: "url",
    },
  ],
};
