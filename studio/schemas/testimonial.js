import { BsChatLeftQuote } from "react-icons/bs";

export default {
  name: "testimonial",
  title: "Testimonial",
  type: "document",
  icon: BsChatLeftQuote,
  fields: [
    {
      name: "name",
      title: "Name",
      type: "string",
    },
    {
        name: "title",
        title: "Title",
        type: "string",
    },
    {
      name: "image",
      title: "Image",
      type: "image",
      options: {
        hotspot: true,
      },
    },
    {
      name: "updatedAt",
      title: "Updated at",
      type: "datetime",
    },
    {
        name: "url",
        description: "Source URL",
        title: "Url",
        type: "url",
    },
    {
        name: "body",
        title: "Body",
        type: 'array', 
        of: [{type: 'block'}]
      },
  ],

  preview: {
    select: {
      title: "name",
      subtitle: "title",
      media: "avatar",
    },
    prepare(selection) {
      const { subtitle } = selection;
      return Object.assign({}, selection, {
        subtitle 
      });
    },
  },
};
