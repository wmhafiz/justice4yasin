import {
  orderRankField,
  orderRankOrdering,
} from "@sanity/orderable-document-list";
import { baseLanguage } from "../languageFilterConfig";

export default {
  name: "person",
  title: "Person",
  type: "document",
  orderings: [orderRankOrdering],
  fields: [
    orderRankField({ type: "category" }),
    {
      name: "name",
      title: "Name",
      type: "string",
    },
    {
      name: "localRole",
      title: "Role",
      type: "localeString",
    },
    {
      name: "slug",
      title: "Slug",
      type: "slug",
      options: {
        source: "name",
        maxLength: 96,
      },
    },
    {
      name: "image",
      title: "Image",
      type: "image",
      options: {
        hotspot: true,
      },
    },
    {
      name: "email",
      title: "E-mail",
      type: "string",
    },
    {
      name: "phone",
      title: "Phone No",
      type: "string",
    },
    {
      name: "localBio",
      title: "Bio",
      type: "localeRichText",
    },
  ],
  preview: {
    select: {
      title: "name",
      subtitle: `localRole.${baseLanguage.id}`,
      media: "image",
    },
  },
};
