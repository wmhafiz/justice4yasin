import {
  orderRankField,
  orderRankOrdering,
} from "@sanity/orderable-document-list";

export default {
  name: "gallery",
  title: "Gallery",
  type: "document",
  orderings: [orderRankOrdering],
  fields: [
    orderRankField({ type: "category" }),
    {
      name: "title",
      title: "Title",
      type: "string",
    },
    {
      name: "subtitle",
      title: "Subtitle",
      type: "string",
    },
    {
      name: "image",
      title: "Image",
      type: "image",
      options: {
        hotspot: true,
      },
    },
    {
      name: "fit",
      type: "string",
      title: "Object Fit",
      options: {
        list: [
          { title: "Cover", value: "cover" },
          { title: "Contain", value: "contain" },
          { title: "Fill", value: "fill" },
          { title: "None", value: "none" },
          { title: "Scale Down", value: "scale-down" },
        ],
        layout: "radio",
      },
    },
    {
      name: "position",
      type: "string",
      title: "Object Position",
      options: {
        list: [
          { title: "bottom", value: "bottom" },
          { title: "center", value: "center" },
          { title: "left", value: "left" },
          { title: "left-bottom", value: "left-bottom" },
          { title: "left-top", value: "left-top" },
          { title: "right", value: "right" },
          { title: "right-bottom", value: "right-bottom" },
          { title: "right-top", value: "right-top" },
          { title: "top", value: "top" },
        ],
        layout: "radio",
      },
      default: "center",
    },
    {
      name: "enabled",
      type: "boolean",
      title: "Enabled",
    },
  ],
  preview: {
    select: {
      title: "title",
      subtitle: "subtitle",
      image: "image",
    },
    prepare(selection) {
      const { title, subtitle, image } = selection;

      return {
        title: title,
        subtitle: subtitle,
        media: image,
      };
    },
  },
};
