import { useRouter } from "next/router";
import Link from "next/link";
import { BsFacebook, BsInstagram } from "react-icons/bs";
import { SiGofundme } from "react-icons/si";
import { v4 as uuidv4 } from "uuid";

export default function ComingSoon({
  visible,
  setVisible,
  facebook,
  gofundme,
  instagram,
}) {
  const router = useRouter();
  const locale = router.locale;
  const text = {
    en: `is under heavy construction and we are working around the clock to
      launch it officially by 22nd June 2022. In the mean time, please bear
      with us and check out our social media`,
    ms: `masih didalam pembangunan dan kami sedang berusaha untuk menyiapkan sebelum 
      perlancaran rasmi pada 22 Jun 2022. Sementara itu, anda boleh menyemak
      laman social media kami`,
  };
  return visible ? (
    <div className="fixed left-5 right-5 bottom-10 md:bottom-5 bg-white p-5 rounded-xl shadow-xl">
      <div>
        <strong className="font-bold">Coming Soon</strong>
        <p className="text-sm">
          <a className="underline" href="" target="_blank">
            YasinSulaiman.com
          </a>{" "}
          {locale === "en" ? text.en : text.ms}
        </p>
      </div>
      <button
        onClick={() => setVisible(!visible)}
        className="absolute top-0 right-0 h-16 w-16 mt-2"
      >
        x
      </button>
      <div className="flex flex-col">
        <div className="grid place-items-center mx-auto sm:my-auto bg-white rounded-3xl">
          <div className="flex items-center justify-center space-x-3 mt-4">
            <Link key={uuidv4()} href={facebook} target="_blank">
              <a className="bg-blue-500 hover:bg-blue-400 px-4 py-3 font-semibold text-white inline-flex items-center space-x-2 rounded">
                <BsFacebook />
              </a>
            </Link>
            <Link key={uuidv4()} href={instagram} target="_blank">
              <a className="bg-red-500 hover:bg-red-400 px-4 py-3 font-semibold text-white inline-flex items-center space-x-2 rounded">
                <BsInstagram />
              </a>
            </Link>
            <Link key={uuidv4()} href={gofundme} target="_blank">
              <a className="bg-green-500 hover:bg-green-400 px-4 py-3 font-semibold text-white inline-flex items-center space-x-2 rounded">
                <SiGofundme />
              </a>
            </Link>
          </div>
        </div>
      </div>
    </div>
  ) : (
    <div></div>
  );
}
