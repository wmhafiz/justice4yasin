import Link from "next/link";
import { withRouter } from "next/router";

function Header({
  title,
  tagline1,
  tagline2,
  isNavOpen,
  setIsNavOpen,
  router,
}) {
  const { locale, locales, asPath } = router;
  return (
    <div
      as="header"
      className="bg-white shadow lg:static lg:overflow-y-visible"
    >
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="relative flex justify-between xl:grid xl:grid-cols-12 lg:gap-8">
          <div className="hidden lg:flex md:absolute md:left-0 md:inset-y-0 lg:static xl:col-span-2">
            <div className="flex-shrink-0 flex items-center justify-items-center">
              <Link href="/" locale={locale}>
                <a>
                  <img
                    className="block h-28"
                    src="/logo2.png"
                    alt={`${title || "YasinSulaiman.com"} logo`}
                  />
                </a>
              </Link>
            </div>
          </div>
          <div
            className="block lg:hidden py-10 space-y-2"
            onClick={() => setIsNavOpen((prev) => !prev)}
          >
            <span className="block h-0.5 w-8 animate-pulse bg-gray-600"></span>
            <span className="block h-0.5 w-8 animate-pulse bg-gray-600"></span>
            <span className="block h-0.5 w-8 animate-pulse bg-gray-600"></span>
          </div>
          <div className="min-w-0 flex-1 md:px-8 lg:px-0 xl:col-span-6 my-4">
            <div className="flex flex-col items-center mt-2">
              <h1 className="text-3xl lg:text-7xl font-serif mb-2 text-amber-900">
                {title}
              </h1>
              <h3 className="text-md lg:text-xl font-mono flex  space-x-4">
                <span className="text-amber-700 underline decoration-double decoration-amber-500">
                  {tagline1}
                </span>{" "}
                <span className="text-amber-700 hidden lg:block underline decoration-double decoration-amber-500">
                  {tagline2}
                </span>
              </h3>
            </div>
          </div>
          <div className="hidden sm:flex sm:items-center sm:justify-end xl:col-span-4">
            {locales.map((loc) => (
              <Link key={loc} href={asPath} locale={loc}>
                <a
                  className={
                    loc === locale
                      ? "text-white bg-gradient-to-r from-amber-400 via-amber-500 to-amber-600 hover:bg-gradient-to-br font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
                      : "font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
                  }
                >
                  {loc == "en" ? "English" : "Malay"}
                </a>
              </Link>
            ))}
            {/* <a
              href="#"
              className="hidden sm:inline-flex flex-shrink-0 ml-5 rounded-full p-1 text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-amber-500"
            >
              <span className="sr-only">View notifications</span>
              <BellIcon className="h-6 w-6" aria-hidden="true" />
            </a>

            <Link href="/contact-us">
              <a className="hidden sm:inline-flex ml-6 items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-amber-600 hover:bg-amber-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-amber-500">
                Contact Us
              </a>
            </Link> */}
          </div>
        </div>
      </div>
    </div>
  );
}

export default withRouter(Header);
