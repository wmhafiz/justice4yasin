import { useState } from "react";
import Head from "next/head";
import Header from "./header";
import Navigation from "./nav";
import ComingSoon from "./coming-soon";

export default function Layout({ children, siteSettings, page }) {
  const [isNavOpen, setIsNavOpen] = useState(false);
  const [visible, setVisible] = useState(true);
  return (
    <>
      {siteSettings && (
        <Head>
          <title>
            {siteSettings?.title}: {page}
          </title>
          <meta name="description" content={siteSettings?.title} />
          <link rel="icon" href="/favicon.ico" />
        </Head>
      )}
      <div className="static h-full bg-gray-100">
        <div>
          <Header
            title={siteSettings?.title || "YasinSulaiman.com"}
            tagline1={siteSettings?.tagline1 || "#SolidarityForYasinSulaiman"}
            tagline2={siteSettings?.tagline2 || "#SolidaritiUntukYasinSulaiman"}
            isNavOpen={isNavOpen}
            setIsNavOpen={setIsNavOpen}
          />
          <div className="py-10">
            <div className="max-w-3xl mx-auto sm:px-6 lg:max-w-7xl lg:px-8 lg:grid lg:grid-cols-12 lg:gap-8">
              <div className="lg:block col-span-2">
                <Navigation
                  facebook={siteSettings?.facebook}
                  gofundme={siteSettings?.gofundme}
                  instagram={siteSettings?.instagram}
                  isNavOpen={isNavOpen}
                  setIsNavOpen={setIsNavOpen}
                />
              </div>
              <main className="lg:col-span-10">{children}</main>
            </div>
          </div>
        </div>
        <div className="absolute z-40">
          <ComingSoon
            visible={visible}
            setVisible={setVisible}
            facebook={siteSettings?.facebook}
            gofundme={siteSettings?.gofundme}
            instagram={siteSettings?.instagram}
          />
        </div>
      </div>
    </>
  );
}
