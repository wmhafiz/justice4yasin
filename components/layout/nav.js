import { useRouter } from "next/router";
import Link from "next/link";
import {
  FireIcon,
  HomeIcon,
  TrendingUpIcon,
  UserGroupIcon,
  PencilIcon,
  NewspaperIcon,
  AnnotationIcon,
  AtSymbolIcon,
  PhotographIcon,
} from "@heroicons/react/outline";
import {
  BsFacebook,
  BsInstagram,
  BsFileEarmarkPersonFill,
} from "react-icons/bs";
import { BiMusic } from "react-icons/bi";
import { MdOutlineLocalHospital } from "react-icons/md";
import { SiGofundme } from "react-icons/si";
import { v4 as uuidv4 } from "uuid";

function classNames(...classes) {
  return classes.filter(Boolean).join(" ");
}

const navigation = [
  { ms: "Laman Utama", en: "Home", href: "/", icon: HomeIcon },
  {
    ms: "Biografi",
    en: "Biography",
    href: "/biography",
    icon: BsFileEarmarkPersonFill,
  },
  {
    ms: "Tentang Kes",
    en: "The Case",
    href: "/the-case",
    icon: FireIcon,
  },
  {
    ms: "Bipolar",
    en: "Bipolar",
    href: "/bipolar",
    icon: MdOutlineLocalHospital,
  },
  { ms: "Artikel", en: "Posts", href: "/post", icon: NewspaperIcon },
  {
    ms: "Testimoni",
    en: "Testimonials",
    href: "/testimonials",
    icon: AnnotationIcon,
  },
  {
    ms: "Musik",
    en: "Music",
    href: "/music",
    icon: BiMusic,
  },
  { ms: "Galleri", en: "Gallery", href: "/gallery", icon: PhotographIcon },
  {
    ms: "Tentang Kami",
    en: "About Us",
    href: "/about-us",
    icon: UserGroupIcon,
  },
  {
    ms: "Hubungi Kami",
    en: "Contact Us",
    href: "/contact-us",
    icon: AtSymbolIcon,
  },
];

const isNavActive = (asPath, href) => {
  if (href === "/") {
    return asPath === href;
  } else {
    return asPath.startsWith(href);
  }
};

const MobileMenu = ({ communities, isNavOpen, setIsNavOpen, router }) => {
  return (
    <div className="flex items-center justify-between border-gray-400 pb-4">
      <nav>
        <section className="flex lg:hidden">
          <div className={isNavOpen ? "showMenuNav" : "hideMenuNav"}>
            <div
              className="absolute top-0 right-0 px-8 py-8"
              onClick={() => setIsNavOpen(false)}
            >
              <svg
                className="h-8 w-8 text-gray-600"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <line x1="18" y1="6" x2="6" y2="18" />
                <line x1="6" y1="6" x2="18" y2="18" />
              </svg>
            </div>
            <ul className="flex flex-col items-center justify-between min-h-[250px]">
              {navigation?.map((item) => (
                <Link key={uuidv4()} href={item.href} locale={router.locale}>
                  <a>
                    <li className="border-b border-gray-400 my-8 uppercase">
                      {item[router.locale]}
                    </li>
                  </a>
                </Link>
              ))}
            </ul>
          </div>
        </section>
      </nav>
      <style>{`
      .hideMenuNav {
        display: none;
      }
      .showMenuNav {
        display: block;
        position: absolute;
        width: 100%;
        height: 100vh;
        top: 0;
        left: 0;
        background: white;
        z-index: 10;
        display: flex;
        flex-direction: column;
        justify-content: space-evenly;
        align-items: center;
      }
    `}</style>
    </div>
  );
};

const DesktopMenu = ({ communities, router }) => {
  return (
    <nav
      aria-label="Sidebar"
      className="hidden lg:block sticky top-4 divide-y divide-gray-300"
    >
      <div className="pb-8 space-y-1">
        {navigation &&
          navigation.map((item) => (
            <Link key={uuidv4()} href={item.href} locale={router.locale}>
              <a
                className={classNames(
                  isNavActive(router.asPath, item.href)
                    ? "bg-amber-100 text-amber-600 shadow rounded"
                    : "text-gray-600 hover:bg-gray-50",
                  "group flex items-center px-3 py-2 text-sm font-medium rounded-md"
                )}
                aria-current={
                  isNavActive(router.asPath, item.href) ? "page" : undefined
                }
              >
                <item.icon
                  className={classNames(
                    isNavActive(router.asPath, item.href)
                      ? "text-amber-500"
                      : "text-gray-400 group-hover:text-gray-500",
                    "flex-shrink-0 -ml-1 mr-3 h-6 w-6"
                  )}
                  aria-hidden="true"
                />
                <span className="truncate">{item[router.locale]}</span>
              </a>
            </Link>
          ))}
      </div>
      <div className="pt-10">
        <p
          className="px-3 text-xs font-semibold text-gray-500 uppercase tracking-wider"
          id="communities-headline"
        >
          Communities
        </p>
        <div
          className="mt-3 space-y-2 mb-10"
          aria-labelledby="communities-headline"
        >
          {communities.map((community) => (
            <a
              key={uuidv4()}
              href={community.href}
              target="_blank"
              className="group flex items-center px-3 py-2 text-sm font-medium text-gray-600 rounded-md hover:text-gray-900 hover:bg-gray-50"
              rel="noreferrer"
            >
              <community.icon
                className={
                  "text-gray-400 group-hover:text-gray-500 flex-shrink-0 -ml-1 mr-3 h-6 w-6"
                }
                aria-hidden="true"
              />
              <span className="truncate">{community.name}</span>
            </a>
          ))}
        </div>
      </div>
    </nav>
  );
};

function Navigation({
  facebook,
  gofundme,
  instagram,
  isNavOpen,
  setIsNavOpen,
}) {
  const communities = [
    {
      name: "Facebook",
      icon: BsFacebook,
      href: facebook,
    },
    { name: "Instagram", icon: BsInstagram, href: instagram },
    { name: "Gofundme", icon: SiGofundme, href: gofundme },
  ];
  const router = useRouter();

  return (
    <>
      <MobileMenu
        communities={communities}
        isNavOpen={isNavOpen}
        setIsNavOpen={setIsNavOpen}
        router={router}
      />
      <DesktopMenu communities={communities} router={router} />
    </>
  );
}

export default Navigation;
