import { PortableText } from "@portabletext/react";
import Link from "next/link";
import { urlFor, myPortableTextComponents } from "../../lib/sanity";

export default function LatestPosts({ newestPosts }) {
  return (
    <section aria-labelledby="trending-heading">
      <div className="bg-white rounded-lg shadow mx-2">
        <div className="p-6">
          <h2
            id="trending-heading"
            className="text-base font-medium text-gray-900"
          >
            Latest Posts
          </h2>
          <div className="mt-6 flow-root">
            <ul role="list" className="-my-4 divide-y divide-gray-200">
              {newestPosts?.map((post) => (
                <Link key={post._id} href={post.url}>
                  <a>
                    <li className="flex py-4 space-x-3 hover:bg-amber-100">
                      <div className="flex-shrink-0">
                        <img
                          className="h-8 w-8 rounded-full"
                          src={urlFor(post.author.image).url()}
                          alt={post.author.name}
                        />
                      </div>
                      <div className="min-w-0 flex-1">
                        <p className="text-md  text-gray-800">{post.title}</p>
                        <div className="text-sm font-light text-gray-500">
                          <PortableText
                            value={post.excerpt}
                            components={myPortableTextComponents}
                          />
                        </div>
                      </div>
                    </li>
                  </a>
                </Link>
              ))}
            </ul>
          </div>
          <div className="mt-6">
            <a
              href="#"
              className="w-full block text-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
            >
              View all
            </a>
          </div>
        </div>
      </div>
    </section>
  );
}
