import { SiGofundme } from "react-icons/si";
import Link from "next/link";

export default function GoFundMe() {
  return (
    <section aria-labelledby="trending-heading">
      <div className="bg-white rounded-lg shadow mx-2">
        <div className="p-6">
          <div className="flex items-center">
            <SiGofundme />{" "}
            <h2
              id="trending-heading"
              className="ml-4 text-base font-medium text-gray-900"
            >
              GoFundMe Campaign
            </h2>
          </div>
          <iframe
            className="gfm-embed h-64 mt-4"
            src="https://www.gofundme.com/f/by7hs-freedom-for-yasin-sulaiman/widget/medium/"
          ></iframe>
          <div>
            <Link href="/post/launching-gofundme">
              <a className="w-full block text-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50">
                View post
              </a>
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
}
