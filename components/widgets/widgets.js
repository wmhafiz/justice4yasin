import FacebookPage from "./facebook-page";
import GoFundMe from "./go-fund-me";
import FeaturedPosts from "./post-featured";
import LatestPosts from "./post-latest";
import WhoToFollow from "./who-to-follow";

export default function Widgets({
  whoToFollow = false,
  featuredPost = false,
  latestPost = true,
  featuredPosts,
  newestPosts,
}) {
  return (
    <div className="flex flex-col md:flex-row gap-6 md:gap-0 ">
      <GoFundMe />
      <FacebookPage />
      {latestPost && (
        <div className="hidden lg:block">
          <LatestPosts newestPosts={newestPosts} />
        </div>
      )}
      {whoToFollow && <WhoToFollow />}
      {featuredPost && <FeaturedPosts featuredPosts={featuredPosts} />}
    </div>
  );
}
