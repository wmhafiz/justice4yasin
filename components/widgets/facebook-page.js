import { FaFacebookF } from "react-icons/fa";

export default function FacebookPage({ width = 300, height = 225 }) {
  return (
    <section aria-labelledby="trending-heading mb-4">
      <div className="bg-white rounded-lg shadow mx-2">
        <div className="p-6">
          <div className="flex items-center mb-4">
            <FaFacebookF />{" "}
            <h2
              id="trending-heading"
              className="ml-4 text-base font-medium text-gray-900"
            >
              Facebook Page
            </h2>
          </div>
          <iframe
            className="rounded-lg"
            src={`https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fyasinsulaimanofficial&tabs=timeline&width=${width}&height=${height}&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=650085618343609`}
            width={width}
            height={height}
            style={{ border: "none", overflow: "hidden" }}
            scrolling="no"
            frameBorder="0"
            allowFullScreen={true}
            allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"
          ></iframe>
          <div>
            <a
              target="_blank"
              href="https://www.facebook.com/yasinsulaimanofficial"
              className="mt-8 w-full block text-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
              rel="noreferrer"
            >
              Visit FB Page
            </a>
          </div>
        </div>
      </div>
    </section>
  );
}
