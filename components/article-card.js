export default function ArticleCard({ title, siteName, url, imageUrl }) {
  return (
    <div>
      <a href={url} target="_blank" rel="noreferrer">
        <figure className="md:flex bg-slate-100 rounded-xl">
          <img
            className="w-full md:w-48 md:object-cover overflow-hidden"
            src={imageUrl}
            alt=""
          />
          <div className="md:pt-6 text-center md:text-left ">
            <blockquote>
              <p className="text-lg font-medium">{title}</p>
            </blockquote>
            <figcaption className="font-medium">
              <div className="text-sky-500 dark:text-sky-400 ml-4 pb-6">
                {siteName}
              </div>
            </figcaption>
          </div>
        </figure>
      </a>
    </div>
  );
}
