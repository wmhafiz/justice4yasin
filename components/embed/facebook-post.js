export default function FacebookPostPreview({
  url,
  width = 500,
  height = 800,
}) {
  if (!url) {
    return null;
  }
  const encodedUrl = encodeURIComponent(url);
  return (
    <iframe
      className="w-full aspect-auto"
      src={`https://www.facebook.com/plugins/post.php?href=${encodedUrl}&width=${width}&show_text=true&appId=650085618343609&height=${height}`}
      width={width}
      height={height}
      style={{ border: "none", overflow: "hidden" }}
      scrolling="no"
      frameBorder="0"
      allowFullScreen={true}
      allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"
    ></iframe>
  );
}
