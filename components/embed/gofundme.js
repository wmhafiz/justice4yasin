export default function GoFundMePreview({
  url,
  size,
  width = 500,
  height = 500,
}) {
  if (!url) {
    return null;
  }
  return (
    <iframe
      className="gfm-embed w-full aspect-auto"
      width={width}
      height={height}
      src={`${url}/widget/${size}/`}
    ></iframe>
  );
}
