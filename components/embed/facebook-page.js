export default function FacebookPagePreview({
  url,
  title = "Facebook Page",
  width = 500,
  height = 800,
}) {
  if (!url) {
    return null;
  }
  const encodedUrl = encodeURIComponent(url);
  return (
    <iframe
      className="w-full aspect-auto"
      src={`https://www.facebook.com/plugins/page.php?href=${encodedUrl}&tabs=timeline&width=${width}&height=${height}&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=650085618343609`}
      width={width}
      height={height}
      style={{ border: "none", overflow: "hidden" }}
      scrolling="no"
      frameBorder="0"
      allowFullScreen={true}
      allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"
    ></iframe>
  );
}
