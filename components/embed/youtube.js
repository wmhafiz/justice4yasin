import getVideoId from "get-video-id";

export default function YoutubePreview({
  url,
  title = "YouTube video player",
  frameBorder = 0,
  allowFullScreen = true,
}) {
  const { id } = getVideoId(url);
  return (
    <iframe
      className="w-full h-96 aspect-video"
      src={`https://www.youtube.com/embed/${id}`}
      title={title}
      frameBorder={frameBorder}
      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
      allowFullScreen={allowFullScreen}
    ></iframe>
  );
}
