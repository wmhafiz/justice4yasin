import {
  createPreviewSubscriptionHook,
  createCurrentUserHook,
} from "next-sanity";
import createImageUrlBuilder from "@sanity/image-url";
import { config } from "./config";
import YoutubePreview from "../components/embed/youtube";
import ArticleCard from "../components/article-card";
import FacebookPagePreview from "../components/embed/facebook-page";
import FacebookPostPreview from "../components/embed/facebook-post";
import GoFundMePreview from "../components/embed/gofundme";

/**
 * Set up a helper function for generating Image URLs with only the asset reference data in your documents.
 * Read more: https://www.sanity.io/docs/image-url
 **/
export const urlFor = (source) => createImageUrlBuilder(config).image(source);

// Set up the live preview subscription hook
export const usePreviewSubscription = createPreviewSubscriptionHook(config);

// Helper function for using the current logged in user account
export const useCurrentUser = createCurrentUserHook(config);

export const myPortableTextComponents = {
  types: {
    "facebook-page": (props) => (
      <FacebookPagePreview
        url={props.value.url}
        width={props.value.width}
        height={props.value.height}
      />
    ),
    "facebook-post": (props) => (
      <FacebookPostPreview
        url={props.value.url}
        width={props.value.width}
        height={props.value.height}
      />
    ),
    gofundme: (props) => (
      <GoFundMePreview url={props.value.url} size={props.value.size} />
    ),
    youtube: (props) => <YoutubePreview url={props.value.url} />,
    article: ({ value }) => (
      <div className="aspect-w-16 aspect-h-9">
        {/* <pre>{JSON.stringify(value, null, 2)}</pre> */}
        <ArticleCard
          title={value.url.openGraph.title}
          siteName={value.url.openGraph.siteName}
          url={value.url.openGraph.url}
          imageUrl={value.url.openGraph.image}
        />
      </div>
    ),
    image: ({ value }) => (
      <div className="aspect-w-16 aspect-h-9">
        <img src={urlFor(value.asset).url()} alt="" />
      </div>
    ),
  },

  marks: {
    span: (props) => <pre>{JSON.stringify(props, null, 2)}</pre>,
    em: ({ children }) => (
      <em className="text-gray-600 font-semibold">{children}</em>
    ),
    internalLink: (props) => {
      // return <pre>{JSON.stringify(props, null, 2)}</pre>;
      const { value, children } = props;
      const postUrl = `/post/${value?.slug.current}`;
      return (
        <a
          className="text-amber-600 italic font-semibold hover:underline"
          href={postUrl}
        >
          {children}
        </a>
      );
    },
    link: (props) => {
      const { value, children } = props;
      return (
        <a
          className="text-amber-600 italic font-semibold hover:underline"
          href={value?.href}
        >
          {children}
        </a>
      );
    },
  },

  block: {
    h1: ({ children }) => (
      <h1 className="text-4xl font-semibold">{children}</h1>
    ),
    h2: ({ children }) => (
      <h2 className="text-3xl font-semibold">{children}</h2>
    ),
    h3: ({ children }) => (
      <h3 className="text-2xl font-semibold">{children}</h3>
    ),
    h4: ({ children }) => <h4 className="text-xl font-light">{children}</h4>,
    blockquote: ({ children }) => (
      <blockquote className="border-l-purple-500">{children}</blockquote>
    ),
  },
};

/**
 * Helper function to return the correct version of the document
 * If we're in "preview mode" and have multiple documents, return the draft
 */
export function filterDataToSingleItem(data, preview) {
  if (!Array.isArray(data)) {
    return data;
  }

  if (data.length === 1) {
    return data[0];
  }

  if (preview) {
    return data.find((item) => item._id.startsWith(`drafts.`)) || data[0];
  }

  return data[0];
}
