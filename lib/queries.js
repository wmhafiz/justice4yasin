import { groq } from "next-sanity";

export const postQuery = groq`
*[_type == "page"][0] {
  _id,
  title,
  body,
  mainImage,
  "slug": slug.current
}
`;
